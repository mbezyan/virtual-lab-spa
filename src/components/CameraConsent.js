import React from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";

export default class CameraConsent extends React.Component {
  render() {
    return (
      <div>
        <h1>Webcam feed capture consent</h1>
        <p>
          Please provide your consent to have your webcam video feed recorded
          during this session.
        </p>
        <br />
        <Link to="/lab/camera-test">
          <Button className="ui primary button">
            <p>I agree</p>
          </Button>
        </Link>
      </div>
    );
  }
}
