import React from "react";
import { useNavigate } from "react-router-dom";
import Webcam from "react-webcam";
import bff from "../api/bff";
import UserContext from "../contexts/UserContext";

class CameraTestClass extends React.Component {
  static contextType = UserContext;

  onFormSubmit = async (event) => {
    const { personId } = this.context;
    const { setSessionId } = this.context;

    event.preventDefault();
    // Note that in a Class-based component we use this.props instead of props used in Functional components
    // this.props.onSubmit(this.state.term);

    const response = await bff.post("lab-sessions", {
      webcamCaptureConsent: true,
      personId: personId,
    });

    setSessionId(response.data);
  };

  render() {
    const { navigate } = this.props;

    return (
      <div>
        <h1>Test your webcam</h1>
        <p>To help us detect your blink with a high accuracy please:</p>
        <ul>
          <li>Face your webcam when playing</li>
          <li>Ensure there is enough light</li>
          <li>Look at your screen as much as possible while playing</li>
        </ul>
        The following feed from your webcam will help us that we can clearly see
        your eyes.
        <center>
          <div className="ui segment">
            <Webcam />
          </div>
        </center>
        <br />
        <p>
          On the next page you will start your first activity while your webcam
          feed is being captured. Please remember to face your camera and look
          at your screen as much as possible.
        </p>
        <button
          className="ui primary button"
          onClick={(event) => {
            this.onFormSubmit(event).then(() =>
              navigate("/lab/tasks/video-cartoon")
            );
          }}
        >
          <p>I am ready to start</p>
        </button>
      </div>
    );
  }
}

// Wrap and export
export default function CameraTest(props) {
  const navigate = useNavigate();

  return <CameraTestClass {...props} navigate={navigate} />;
}
