import React from "react";
import { Outlet } from "react-router-dom";
import bff from "../api/bff";
import UserContext from "../contexts/UserContext";

export default class Lab extends React.Component {
  state = { personId: null, sessionId: null };

  componentWillUnmount() {
    // TODO: Mark session as ended here
    console.log("Lab.componentWillUnmount()");
    bff.put(`/lab-sessions/${this.state.sessionId}`, { endTs: Date.now() });
  }

  render() {
    console.log("Render: " + JSON.stringify(this.state));
    return (
      <div>
        <UserContext.Provider
          value={{
            personId: this.state.personId,
            sessionId: this.state.sessionId,
            setPersonId: (pid) => this.setState({ personId: pid }),
            setSessionId: (sid) => this.setState({ sessionId: sid }),
          }}
        >
          <Outlet />
        </UserContext.Provider>
      </div>
    );
  }
}
