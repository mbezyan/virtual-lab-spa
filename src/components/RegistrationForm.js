import React from "react";
import Dropdown from "./Dropdown";
import { useNavigate } from "react-router-dom";
import bff from "../api/bff";
import UserContext from "../contexts/UserContext";

class RegistrationFormClass extends React.Component {
  state = {
    username: "",
    yearOfBirth: "",
    gender: "not-specified",
    race: "not-specified",
  };

  static contextType = UserContext;

  onFormSubmit = async (event) => {
    const { setPersonId } = this.context;
    event.preventDefault();
    // Note that in a Class-based component we use this.props instead of props used in Functional components
    // this.props.onSubmit(this.state.term);

    await bff
      .post("persons", {
        username: this.state.username,
        yearOfBirth: this.state.yearOfBirth,
        gender: this.state.gender.value,
        race: this.state.race.value,
      })
      .then((response) => {
        console.log("Will call setPersonId: " + response.data);
        setPersonId(response.data);
      });
  };

  render() {
    const { navigate } = this.props;
    return (
      <div>
        <h1>Your details</h1>
        <h2>Why do we ask for this information?</h2>
        <p>This will help us with data analysis.</p>
        <div className="ui four column grid">
          <div className="column">
            <form onSubmit={this.onFormSubmit} className="ui form">
              <div className="field">
                <label>Username</label>
                <input
                  type="text"
                  value={this.state.username}
                  onChange={(e) => this.setState({ username: e.target.value })}
                />
              </div>
              <div className="field">
                <label>Year of Birth</label>
                <input
                  type="text"
                  value={this.state.yearOfBirth}
                  onChange={(e) =>
                    this.setState({ yearOfBirth: e.target.value })
                  }
                />
              </div>
              <Dropdown
                label="Gender"
                selected={this.state.gender}
                options={genders}
                onSelectedChange={(selection) =>
                  this.setState({ gender: selection })
                }
              />
              <br />
              <Dropdown
                label="Race (something more appropriate)"
                selected={this.state.race}
                options={races}
                onSelectedChange={(selection) =>
                  this.setState({ race: selection })
                }
              />
              <br />
              <button
                className="ui primary button"
                onClick={(event) => {
                  this.onFormSubmit(event).then(() =>
                    navigate("/lab/camera-consent")
                  );
                }}
              >
                <p>Register and continue</p>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const genders = [
  {
    value: "female",
    label: "Female",
  },
  {
    value: "male",
    label: "Male",
  },
  {
    value: "not-specified",
    label: "Rather not say",
  },
];

const races = [
  {
    value: "asian-indian",
    label: "Asian Indian",
  },
  {
    value: "black",
    label: "Black",
  },
  {
    value: "caucasian",
    label: "Caucasian",
  },
  {
    value: "east-asian",
    label: "East Asian",
  },
  {
    value: "not-specified",
    label: "Rather not say",
  },
];

// Wrap and export
export default function RegistrationForm(props) {
  const navigate = useNavigate();

  return <RegistrationFormClass {...props} navigate={navigate} />;
}
