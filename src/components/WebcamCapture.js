import axios from "axios";
import React from "react";
import Webcam from "react-webcam";
import UserContext from "../contexts/UserContext";

const FRAMES_PER_SECOND = 30;
const MILLIS_IN_SEC = 1000;
const IMG_CAPTURE_INTERVAL = Math.floor(MILLIS_IN_SEC / FRAMES_PER_SECOND);
const IMG_BUFFER_SIZE = 60;

class WebcamCapture extends React.Component {
  imageBuffer = [];
  webcamCaptureIntervalId = null;
  static contextType = UserContext;
  sessionId = null;
  frameNumber = 0;

  constructor(props) {
    super(props);
    this.webcamRef = React.createRef();
  }

  persistImages = async (imageBuffer) => {
    axios.post("http://localhost:3001/observations", {
      sessionId: this.sessionId,
      images: imageBuffer,
    });
  };

  componentDidMount() {
    this.sessionId = this.context.sessionId;
    const intervalId = setInterval(() => {
      if (this.webcamRef.current) {
        const imageSrc = this.webcamRef.current.getScreenshot();
        if (imageSrc) {
          this.imageBuffer.push({
            frameNumber: ++this.frameNumber,
            timestamp: Date.now(),
            image: imageSrc.split(",")[1],
          });
          if (this.imageBuffer.length >= IMG_BUFFER_SIZE) {
            this.persistImages(this.imageBuffer);
            // Creating a new array and assigning to imageBuffer to leave array passed to async call intact
            this.imageBuffer = [];
          }
        }
      } else {
        console.log(intervalId + ": I am lost");
      }
    }, IMG_CAPTURE_INTERVAL);
    this.webcamCaptureIntervalId = intervalId;
  }

  componentWillUnmount() {
    console.log(
      "componentWillUnmount(): with " +
        this.imageBuffer?.length +
        " images in imageBuffer"
    );
    clearInterval(this.webcamCaptureIntervalId);
    if (this.imageBuffer?.length > 0) {
      this.persistImages(this.imageBuffer);
    }
  }

  render() {
    return (
      <>
        <Webcam
          style={{ position: "absolute", right: "100%" }}
          audio={false}
          ref={this.webcamRef}
          screenshotFormat="image/jpg"
          videoConstraints={{ frameRate: 30, height: 720, width: 1280 }}
        />
      </>
    );
  }
}

export default WebcamCapture;
