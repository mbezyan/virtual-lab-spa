import React from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import MatchingPairs from "./borrowed/MatchingPairs";

export default class MatchingPairsContainer extends React.Component {
  render() {
    return (
      <div>
        <h1>Matching Pairs</h1>
        <center>
          <MatchingPairs />
        </center>
        <br />
        <br />
        <Link to="/lab/tasks/game-two">
          <Button className="ui button primary">
            <p>Next game</p>
          </Button>
        </Link>
      </div>
    );
  }
}
