import { Outlet } from "react-router-dom";

export default function MatchingPairsOutlet() {
  return (
    <div>
      <Outlet />
    </div>
  );
}
