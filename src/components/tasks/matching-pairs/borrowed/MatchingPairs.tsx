import { useState, useContext, useEffect } from "react";

import Score from "./Score";
import Board from "./Board";
import LabSessionContext from "../../../../contexts/LabSessionContext";
import TaskInteractionsContext from "../../../../contexts/TaskInteractionsContext";
import { generateUUID } from "../../../../common/utils";

const cardIds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
const minMovesToComplete: number = cardIds.length / 2;
cardIds.sort(() => 0.5 - Math.random());

function MatchingPairs() {
  const labSessionContext = useContext(LabSessionContext);
  const { currentTaskId, closeCurrentTask } = labSessionContext;
  const taskInteractionsContext = useContext(TaskInteractionsContext);
  const { upsertTaskInteraction, persistTaskInteractions } =
    taskInteractionsContext;
  const [playingGameTiid, setPlayingGameTiid] = useState();

  const [moves, setMoves] = useState<number>(0);
  const [bestScore, setBestScore] = useState<number>(
    parseInt(localStorage.getItem("bestScore") || "0") ||
      Number.MAX_SAFE_INTEGER
  );

  useEffect(() => {
    // Create new playing-game interaction
    const tiid = generateUUID();
    upsertTaskInteraction({
      id: tiid,
      taskId: currentTaskId,
      type: "playing-game",
      startTs: Date.now(),
    });
    setPlayingGameTiid(tiid);

    return () => {
      persistTaskInteractions();
    };
  }, []);

  const finishGameCallback = () => {
    const newBestScore = moves < bestScore ? moves : bestScore;
    setBestScore(newBestScore);
    localStorage.setItem("bestScore", "" + newBestScore);
    upsertTaskInteraction({
      id: playingGameTiid,
      endTs: Date.now(),
    });
    closeCurrentTask({ completed: true, score: minMovesToComplete - moves });
  };

  return (
    <div className="app-container">
      <Score moves={moves} bestScore={bestScore} />
      <Board
        setMoves={setMoves}
        finishGameCallback={finishGameCallback}
        cardIds={cardIds}
      />
    </div>
  );
}

export default MatchingPairs;
