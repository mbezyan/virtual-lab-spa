import { useEffect, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";

import LabSessionContext from "../../../contexts/LabSessionContext";
import TaskInteractionsContext from "../../../contexts/TaskInteractionsContext";

export default function MatchingPairsIntro() {
  const navigate = useNavigate();
  const labSessionContext = useContext(LabSessionContext);
  const taskInteractionsContext = useContext(TaskInteractionsContext);
  const { createTaskAttemptAndInitialInteraction } = labSessionContext;
  const { upsertTaskInteraction, persistTaskInteractions } =
    taskInteractionsContext;
  const [readingInstructionsTiid, setReadingInstructionsTiid] = useState();

  useEffect(() => {
    // persistTaskInteractions(); // Persist interactions for the previous task

    async function createTaskAttempt() {
      // Create new Task Attempt
      const { readingInstructionsTiid } =
        await createTaskAttemptAndInitialInteraction("matching-pairs");
      setReadingInstructionsTiid(readingInstructionsTiid);
    }
    createTaskAttempt();
  }, []);

  const onFormSubmit = (event) => {
    event.preventDefault();
    upsertTaskInteraction({
      id: readingInstructionsTiid,
      endTs: Date.now(),
    });
  };

  return (
    <div className="ui two column grid">
      <div className="column">
        <h1>Matching Pairs</h1>
        <p>
          (The description below is based on an article from Wikipedia, the free
          encyclopedia)
        </p>
        <p>
          Matching Pairs also known as Concentration, Match Match, Match Up,
          Memory, Pleonasm, Shank, Pexeso or simply Pairs, is a card game in
          which all of the cards are laid face down on a surface and two cards
          are flipped face up over each turn. The object of the game is to find
          all matching cards in as little moves as possible. In the best case
          scenario all matches can be found with moves equal to half the number
          of cards. Each move consists of revealing two cards.
        </p>
        <br />
        <button
          className="ui primary button"
          onClick={(event) => {
            onFormSubmit(event);
            navigate("/lab/tasks/matching-pairs/play");
          }}
        >
          <p>Play</p>
        </button>
      </div>
    </div>
  );
}
