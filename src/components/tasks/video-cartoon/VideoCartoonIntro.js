import { useEffect, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";

import LabSessionContext from "../../../contexts/LabSessionContext";
import TaskInteractionsContext from "../../../contexts/TaskInteractionsContext";

export default function VideoCartoonIntro() {
  const navigate = useNavigate();
  const labSessionContext = useContext(LabSessionContext);
  const taskInteractionsContext = useContext(TaskInteractionsContext);
  const { createTaskAttemptAndInitialInteraction } = labSessionContext;
  const { upsertTaskInteraction } = taskInteractionsContext;
  const [readingInstructionsTiid, setReadingInstructionsTiid] = useState();

  useEffect(() => {
    async function createTaskAttempt() {
      // Create new Task Attempt
      const { readingInstructionsTiid } =
        await createTaskAttemptAndInitialInteraction("video-cartoon");
      setReadingInstructionsTiid(readingInstructionsTiid);
    }
    createTaskAttempt();
  }, []);

  const onFormSubmit = (event) => {
    event.preventDefault();
    upsertTaskInteraction({
      id: readingInstructionsTiid,
      endTs: Date.now(),
    });
  };

  return (
    <div className="ui two column grid">
      <div className="column">
        <h1>Watch a short video</h1>
        <p>
          The first task is to watch a short video about 3-4 minutes long.
          Please watch the video to the end and do not pause while watching.
          Once you have finished watching the video, you will be able to go to
          the next task.
        </p>
        <h2>What is the video?</h2>
        <p>
          It is a short animation called The Egyptian Pyramids. It was made by
          Corentin Charron, Lise Corriol, Olivier Lafay and Nicolas Mrikhi for
          their 4th year student project. They made it in back in 2013 in
          Supinfocom Arles (MOPA) for an exhibition at the MuCEM museum in
          Marseille.
        </p>
        <br />
        <button
          className="ui primary button"
          onClick={(event) => {
            onFormSubmit(event);
            navigate("/lab/tasks/video-cartoon/watch");
          }}
        >
          <p>Watch</p>
        </button>
      </div>
    </div>
  );
}
