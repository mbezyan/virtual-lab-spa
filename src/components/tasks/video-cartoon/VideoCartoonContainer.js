import React from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import VideoCartoon from "./borrowed/VideoCartoon";

export default class VideoCartoonContainer extends React.Component {
  render() {
    return (
      <div>
        <h1>The Egyptian Pyramids - Funny Animated Short Film</h1>
        <center>
          <VideoCartoon />
        </center>
        <br />
        <br />
        <Link to="/lab/tasks/matching-pairs">
          <Button className="ui button primary">
            <p>Next task</p>
          </Button>
        </Link>
      </div>
    );
  }
}
