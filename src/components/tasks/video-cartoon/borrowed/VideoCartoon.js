import { useState, useContext, useEffect } from "react";

import LabSessionContext from "../../../../contexts/LabSessionContext";
import TaskInteractionsContext from "../../../../contexts/TaskInteractionsContext";
import { generateUUID } from "../../../../common/utils";

export default function VideoCartoon() {
  const labSessionContext = useContext(LabSessionContext);
  const { currentTaskId, closeCurrentTask } = labSessionContext;
  const taskInteractionsContext = useContext(TaskInteractionsContext);
  const { upsertTaskInteraction, persistTaskInteractions } =
    taskInteractionsContext;
  const [watchingVideoTiid, setWatchingVideoTiid] = useState();

  useEffect(() => {
    // Create new playing-game interaction
    const tiid = generateUUID();
    upsertTaskInteraction({
      id: tiid,
      taskId: currentTaskId,
      type: "watching-video",
      startTs: Date.now(),
    });
    setWatchingVideoTiid(tiid);

    // Wait for video to finish and then allow user to navigate away
    // TODO

    return () => {
      upsertTaskInteraction({
        id: tiid,
        endTs: Date.now(),
      });
      closeCurrentTask({ completed: true });
    };
  }, []);

  return (
    <div>
      <div className="ui embed">
        Video is here
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/j6PbonHsqW0?controls=0&autoplay=1"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        />
      </div>
    </div>
  );
}
