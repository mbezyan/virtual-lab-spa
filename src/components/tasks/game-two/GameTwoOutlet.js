import React from "react";
import { Outlet } from "react-router-dom";

export default class GameTwoOutlet extends React.Component {
  render() {
    return (
      <div>
        <Outlet />
      </div>
    );
  }
}
