import React from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";

export default class GameTwoIntro extends React.Component {
  render() {
    return (
      <div>
        <h1>Game Two</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
        <br />
        <Link to="/lab/tasks/game-two/play">
          <Button className="ui button primary">
            <p>Play</p>
          </Button>
        </Link>
      </div>
    );
  }
}
