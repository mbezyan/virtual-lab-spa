import React from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import GameTwo from "./borrowed/GameTwo";

export default class GameTwoContainer extends React.Component {
  render() {
    return (
      <div>
        <h1>Matching Pairs</h1>
        <center>
          <GameTwo />
        </center>
        <br />
        <br />
        <Link to="/lab/tasks/astroids">
          <Button className="ui button primary">
            <p>Next game</p>
          </Button>
        </Link>
      </div>
    );
  }
}
