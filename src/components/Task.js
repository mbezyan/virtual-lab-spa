import React from "react";
import { Outlet } from "react-router-dom";
import LabSessionContext from "../contexts/LabSessionContext";
import TaskInteractionsContext from "../contexts/TaskInteractionsContext";
import bff from "../api/bff";
import UserContext from "../contexts/UserContext";
import { generateUUID } from "../common/utils";

export default class Task extends React.Component {
  state = { currentTaskId: null, taskInteractions: [] };
  mediaRecorder = null;
  mediaRecorderIntervalId = null;
  videoStartTs = 0;
  videoEndTs = 0;
  static contextType = UserContext;
  sessionId = this.context.sessionId;

  // Form Data config
  config = {
    headers: {
      "Content-Type": "multipart/form-data; boundary=${data._boundary}",
    },
  };

  setCurrentTaskId = (taskId) => this.setState({ currentTaskId: taskId });

  createTaskAttemptAndInitialInteraction = async (taskName) => {
    console.log("createTaskAttemptAndInitialInteraction");
    console.log(taskName);
    let taskId = null;
    let readingInstructionsTiid = null;

    // Create new Task Attempt
    const response = await bff.post("/task-attempts", {
      sessionId: this.sessionId,
      taskName: taskName,
    });
    taskId = response.data;

    // Create new reading-instructions interaction
    readingInstructionsTiid = generateUUID();
    this.upsertTaskInteraction({
      id: readingInstructionsTiid,
      taskId: taskId,
      type: "reading-instructions",
      startTs: Date.now(),
    });

    this.setCurrentTaskId(taskId);

    return { taskId, readingInstructionsTiid };
  };

  upsertTaskInteraction = (taskInteraction) => {
    console.log(JSON.stringify(taskInteraction));
    const currentTaskInteractions = this.state.taskInteractions;
    let udpatedTaskInteractions = null;
    const foundIndex = currentTaskInteractions.findIndex(
      (item) => item.id === taskInteraction.id
    );
    if (foundIndex > -1) {
      // Update
      console.log("Updating");
      udpatedTaskInteractions = currentTaskInteractions.map((item) => {
        if (item.id === taskInteraction.id) {
          return { ...item, ...taskInteraction };
        } else {
          return item;
        }
      });
    } else {
      // Push
      console.log("Inserting");
      udpatedTaskInteractions = [...currentTaskInteractions, taskInteraction];
    }
    this.setState({ taskInteractions: udpatedTaskInteractions });
  };

  persistTaskInteractions = async () => {
    console.log("persistTaskInteractions:");
    console.log(JSON.stringify(this.state.taskInteractions));
    await bff.post("/task-interactions", this.state.taskInteractions);
    this.setState({ taskInteractions: [] });
  };

  closeCurrentTask = async (taskPerformance) => {
    bff.put(`/task-attempts/${this.state.currentTaskId}`);
    bff.post("/task-performances", {
      ...taskPerformance,
      ...{ taskAttemptId: this.state.currentTaskId },
    });
  };

  componentDidMount = () => {
    // Start video stream
    const hdConstraints = {
      video: {
        width: { min: 1280 },
        height: { min: 720 },
        frameRate: { min: 30 },
      },
      audio: false,
    };

    navigator.mediaDevices.getUserMedia(hdConstraints).then((stream) => {
      console.log("Streaming started");
      const options = {
        videoBitsPerSecond: 5242880,
        mimeType: "video/webm",
      };
      const recordedChunks = [];
      this.mediaRecorder = new MediaRecorder(stream, options);

      this.mediaRecorder.ondataavailable = (e) => {
        console.log("ondataavailable");
        if (e.data.size > 0) {
          recordedChunks.push(e.data);
          console.log(e.data.size, recordedChunks.length);

          var formData = new FormData();
          formData.append("video", new Blob(recordedChunks));
          formData.append("sessionId", this.sessionId);
          formData.append("startTs", this.videoStartTs);
          formData.append("endTs", this.videoEndTs);

          bff.post("/observations/videos", formData, this.config);
        }
      };

      this.mediaRecorder.onstop = () => {
        console.log("onstop");

        // Stop camera
        const track = stream?.getTracks()[0]; // There is only one video track
        console.log("Stopping track: " + track);
        track.stop();
      };

      this.videoStartTs = Date.now();
      this.mediaRecorder.start();
      this.mediaRecorderIntervalId = setInterval(() => {
        this.videoEndTs = Date.now();
        this.mediaRecorder.requestData();
      }, 20000);
    });
  };

  componentWillUnmount() {
    console.log("Task.componentWillUnmount");
    this.videoFinishTs = Date.now();
    this.mediaRecorder.stop();
    clearInterval(this.mediaRecorderIntervalId);
  }

  render() {
    console.log(this.state);
    return (
      <div>
        <div className="ui visible yellow small message">
          <i className="icon camera"></i>
          We are capturing your webcam feed to analyse your blinks
        </div>
        <br />
        <LabSessionContext.Provider
          value={{
            currentTaskId: this.state.currentTaskId,
            setCurrentTaskId: (ctid) => this.setCurrentTaskId(ctid),
            createTaskAttemptAndInitialInteraction: async (taskName) =>
              this.createTaskAttemptAndInitialInteraction(taskName),
            closeCurrentTask: (taskPerformance) =>
              this.closeCurrentTask(taskPerformance),
          }}
        >
          <TaskInteractionsContext.Provider
            value={{
              taskInteractions: this.state.taskInteractions,
              upsertTaskInteraction: this.upsertTaskInteraction,
              persistTaskInteractions: this.persistTaskInteractions,
            }}
          >
            <Outlet />
          </TaskInteractionsContext.Provider>
        </LabSessionContext.Provider>
      </div>
    );
  }
}
