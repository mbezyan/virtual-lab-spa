import React from "react";
import { NavLink, Outlet } from "react-router-dom";

class App extends React.Component {
  activeMenuStyle({ isActive }) {
    return "item" + (isActive ? " active" : "");
  }

  render() {
    return (
      <div className="ui container" style={{ marginTop: "10px" }}>
        <div className="ui secondary pointing menu">
          <NavLink
            className={(isActive) => this.activeMenuStyle(isActive)}
            to="/"
          >
            Home
          </NavLink>
          <NavLink
            className={(isActive) => this.activeMenuStyle(isActive)}
            to="/lab"
          >
            Virtual Lab
          </NavLink>
          <NavLink
            className={(isActive) => this.activeMenuStyle(isActive)}
            to="/about"
          >
            About
          </NavLink>
          <div className="right menu">
            <h1>MindLab</h1>
          </div>
        </div>
        <div className="ui container">
          <Outlet />
        </div>
      </div>
    );
  }
}

export default App;
