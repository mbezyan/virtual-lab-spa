import React from "react";

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <h1>Welcome to MindLab</h1>
        <p>
          Thank you for visiting. Please click on Virtual Lab above to start.
        </p>
      </div>
    );
  }
}
