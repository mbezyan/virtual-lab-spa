import React from "react";

export default class About extends React.Component {
  render() {
    return (
      <div>
        <h1>What is MindLab?</h1>
        <p>MindLab is a virtual lab for blink analysis.</p>
      </div>
    );
  }
}
