import React from "react";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";

export default class LabIntro extends React.Component {
  render() {
    return (
      <div>
        <h1>Virtual Lab</h1>
        <p>
          Welcome to the MindLab Virtual Lab. Please click Next to continue.
        </p>
        <br />
        <Link to="/lab/register">
          <Button className="ui button primary">
            <p>Next</p>
          </Button>
        </Link>
      </div>
    );
  }
}
