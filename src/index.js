import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import About from "./components/About";
import App from "./components/App";
import CameraConsent from "./components/CameraConsent";
import CameraTest from "./components/CameraTest";
import MatchingPairsContainer from "./components/tasks/matching-pairs/MatchingPairsContainer";
import MatchingPairsIntro from "./components/tasks/matching-pairs/MatchingPairsIntro";
import MatchingPairsOutlet from "./components/tasks/matching-pairs/MatchingPairsOutlet";
import Lab from "./components/Lab";
import LabIntro from "./components/LabIntro";
import Task from "./components/Task";
import Home from "./components/Home";
import RegistrationForm from "./components/RegistrationForm";
import GameTwoOutlet from "./components/tasks/game-two/GameTwoOutlet";
import GameTwoIntro from "./components/tasks/game-two/GameTwoIntro";
import GameTwoContainer from "./components/tasks/game-two/GameTwoContainer";
import VideoCartoonOutlet from "./components/tasks/video-cartoon/VideoCartoonOutlet";
import VideoCartoonIntro from "./components/tasks/video-cartoon/VideoCartoonIntro";
import VideoCartoonContainer from "./components/tasks/video-cartoon/VideoCartoonContainer";

ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />}>
        <Route index element={<Home />} />
        <Route path="about" element={<About />} />
        <Route path="lab" element={<Lab />}>
          <Route index element={<LabIntro />} />
          <Route path="register" element={<RegistrationForm />} />
          <Route path="camera-consent" element={<CameraConsent />} />
          <Route path="camera-test" element={<CameraTest />} />
          <Route path="tasks" element={<Task />}>
            <Route path="video-cartoon" element={<VideoCartoonOutlet />}>
              <Route index element={<VideoCartoonIntro />} />
              <Route path="watch" element={<VideoCartoonContainer />} />
            </Route>
            <Route path="matching-pairs" element={<MatchingPairsOutlet />}>
              <Route index element={<MatchingPairsIntro />} />
              <Route path="play" element={<MatchingPairsContainer />} />
            </Route>
            <Route path="game-two" element={<GameTwoOutlet />}>
              <Route index element={<GameTwoIntro />} />
              <Route path="play" element={<GameTwoContainer />} />
            </Route>
          </Route>
        </Route>
        <Route
          path="*"
          element={
            <main style={{ padding: "1rem" }}>
              <p>There's nothing here!</p>
            </main>
          }
        />
      </Route>
    </Routes>
  </BrowserRouter>,
  document.querySelector("#root")
);
