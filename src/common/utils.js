import { v4 as uuidv4 } from "uuid";

function generateUUID() {
  return uuidv4().replaceAll("-", "");
}

export { generateUUID };
