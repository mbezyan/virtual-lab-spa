import React from "react";

const context = React.createContext({
  taskInteractions: [],
  upsertTaskInteraction: ({}) => {},
  persistTaskInteractions: ({}) => {},
});
export default context;
