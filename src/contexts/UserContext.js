import React from "react";

const context = React.createContext({ personId: null, sessionId: null });
export default context;
