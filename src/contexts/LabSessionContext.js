import React from "react";

const context = React.createContext({
  currentTaskId: null,
  createTaskAttemptAndInitialInteraction: async () => {},
  closeCurrentTask: ({}) => {},
});
export default context;
